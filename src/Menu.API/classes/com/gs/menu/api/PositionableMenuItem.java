/*
 * Copyright (C) 2021 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   Menu.API
 *  Class      :   PositionableMenuItem.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 26, 2021 @ 11:39:55 AM
 *  Modified   :   Oct 26, 2021
 * 
 *  Purpose:     See class JavaDoc comment.
 * 
 *  Revision History:
 * 
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 26, 2021  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.gs.menu.api;

import javax.swing.JMenuItem;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 *
 * @version 1.0.0
 * @since 1.0.0
 */
public class PositionableMenuItem extends JMenuItem implements Comparable<PositionableMenuItem> {

    private int position;
    private boolean separatorBefore;
    private boolean separatorAfter;
    
    public PositionableMenuItem (String name, int position, 
            boolean separatorBefore, boolean separatorAfter) {
        configure(name, position, separatorBefore, separatorAfter);
    }
    
    private void configure(String name, int position, boolean separatorBefore,
            boolean separatorAfter) {
        this.position = position;
        setName(name);
        this.separatorAfter = separatorAfter;
        this.separatorBefore = separatorBefore;
    }
    
    public int getPosition() {
        return position;
    }
    
    public boolean hasSeparatorBefore() {
        return separatorBefore;
    }
    
    public boolean hasSeparatorAfter() {
        return separatorAfter;
    }

    @Override
    public int compareTo(PositionableMenuItem o) {
        return Integer.compare(position, o.getPosition());
    }

}
