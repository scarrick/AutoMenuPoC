/*
 * Copyright (C) 2021 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   Menu.API
 *  Class      :   MenuProvider.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 26, 2021 @ 10:22:06 AM
 *  Modified   :   Oct 26, 2021
 * 
 *  Purpose:     See class JavaDoc comment.
 * 
 *  Revision History:
 * 
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 26, 2021  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.gs.menu.api;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The `MenuProvider` annotation allows classes to provide one or more menus to
 * the application's menu bar. This annotation is `Repeatable`, so multiple
 * `MenuProvider` annotations may decorate a single class, such as:
 * 
 * ```java
 * @MenuProvider(text = "File", name = "fileMenu", position = Integer.MIN)
 * @MenuProvider(text = "Edit", name = "editMenu", position = Integer.MIN + 1)
 * @MenuProvider(text = "View", name = "viewMenu", position = Integer.MIN + 2)
 * @MenuProvider(text = "Tools", name = "toolsMenu", position = Integer.MAX - 1000)
 * @MenuProvider(text = "Help", name = "helpMenu", position = Integer.MAX)
 * public class MyClass {
 * 
 *     // ... class code ...
 * 
 * }
 * ```
 * 
 * In this example, there are five (5) menus being annotated, which the processor
 * will then create at application startup. The `position` values of these menus
 * are set so that no dynamically added menu may be placed to the left of the
 * View menu, nor to the right of the Help menu. This is in deference to the UI
 * standards that have been well-established for a few decades. Since these five
 * menus are being dynamically created themselves, the developer needs to think
 * about the UI standards and comply with them. By doing so, it will ease the
 * user's burden of learning how to use the application.
 * 
 * <em>Note</em>: It is worth noting that any application that allows for modules
 * to be loaded dynamically to increase the feature set of the application could
 * use this example to annotate the application's main window class. Furthermore,
 * that same window class could then have an `@Action` method defined within it
 * for exiting the application, which could be itself decorated with the 
 * `@MenuItemProvider` or `@ToolbarButtonProvider` annotations to create the
 * manner of exiting the application cleanly. Again, the UI standards will need
 * to be taken into consideration for the Exit menu item's and/or toolbar 
 * button's position. Setting it to the same as the File menu's position would 
 * be a good start.
 * 
 * @see Action
 * @see MenuItemProvider
 * @see MenuProvider
 * @see ToolbarButtonProvider
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 *
 * @version 1.0.0
 * @since 1.0.0
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Repeatable(RepeatableMenus.class)
public @interface MenuProvider {
    
    /**
     * The `text` property tells the processor to what value to set the menu's 
     * `text` property.
     * 
     * @return the value for the menu's `text` property
     */
    String text();
    
    /**
     * The `name` property gives the menu a name so that its properties can be
     * set dynamically for internationalization via `ResourceBundle`s.
     * 
     * @return the name for the menu
     */
    String name();
    
    /**
     * The `position` tells the processor where on the menu bar the menu should
     * be placed. Setting this property needs careful consideration due to the
     * need to follow well-established UI standards.
     * 
     * @return the menu position on the menu bar
     */
    int position();
}
