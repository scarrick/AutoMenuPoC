/*
 * Copyright (C) 2021 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   Menu.API
 *  Class      :   PositionableMenu.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 26, 2021 @ 11:36:40 AM
 *  Modified   :   Oct 26, 2021
 * 
 *  Purpose:     See class JavaDoc comment.
 * 
 *  Revision History:
 * 
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 26, 2021  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.gs.menu.api;

import javax.swing.JMenu;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 *
 * @version 1.0.0
 * @since 1.0.0
 */
public class PositionableMenu extends JMenu implements Comparable<PositionableMenu> {
    
    private int position;
    
    public PositionableMenu (String name, String text, int position) {
        super(text);
        setName(name);
        this.position = position;
    }
    
    public int getPosition() {
        return position;
    }

    @Override
    public int compareTo(PositionableMenu o) {
        return Integer.compare(position, o.getPosition());
    }

}
