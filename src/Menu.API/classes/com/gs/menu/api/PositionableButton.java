/*
 * Copyright (C) 2021 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   Menu.API
 *  Class      :   PositionableButton.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 26, 2021 @ 11:46:48 AM
 *  Modified   :   Oct 26, 2021
 * 
 *  Purpose:     See class JavaDoc comment.
 * 
 *  Revision History:
 * 
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 26, 2021  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.gs.menu.api;

import javax.swing.JButton;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 *
 * @version 1.0.0
 * @since 1.0.0
 */
public class PositionableButton extends JButton implements Comparable<PositionableButton> {

    private int position;
    private boolean separatorBefore;
    private boolean separatorAfter;
    
    public PositionableButton (String name, String text, int position,
            boolean separatorBefore, boolean separatorAfter) {
        configure(name, text, position, separatorBefore, separatorAfter);
    }
    
    private void configure(String name, String text, int position,
            boolean separatorBefore, boolean separatorAfter) {
        this.position = position;
        setName(name);
        this.separatorAfter = separatorAfter;
        this.separatorBefore = separatorBefore;
        
        // We only want to set the text if it is not "-1".
        if (!"-1".equals(text)) {
            // The value "-1" denotes that the text property will be set by the
            //+ Action. However, the ToolbarButtonProvider allows for the text
            //+ to be overridden. So, we take care of that here.
            setText(text);
        }
    }
    
    public int getPosition() {
        return position;
    }
    
    public boolean hasSeparatorBefore() {
        return separatorBefore;
    }
    
    public boolean hasSeparatorAfter() {
        return separatorAfter;
    }

    @Override
    public int compareTo(PositionableButton o) {
        return Integer.compare(position, o.getPosition());
    }

}
