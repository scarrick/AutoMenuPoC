/*
 * Copyright (C) 2021 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   Menu.API
 *  Class      :   MenuItemProvider.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 26, 2021 @ 8:52:54 AM
 *  Modified   :   Oct 26, 2021
 * 
 *  Purpose:     See class JavaDoc comment.
 * 
 *  Revision History:
 * 
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 26, 2021  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.gs.menu.api;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Provides a means by which modules can mark an `@Action` as providing a menu
 * item to the application's main menu bar.
 * 
 * When setting up a platform application, the `@Action` annotation is used to
 * decorate a method that will become a `javax.swing.Action` that is assigned to
 * a menu item, toolbar button, or some other button within the interface of the
 * application, such as:
 * 
 * ```java
 * @Action
 * public void doSomething() {
 *     // do processing here...
 * }
 * ```
 * 
 * If it is desired for the `@Action` to have a menu item to execute it, then
 * the `@Action` will need to be decorated in this way:
 * 
 * ```java
 * @MenuItemProvider(
 *         owner = "File",
 *         position = 300
 * )
 * @Action
 * public void doSomething() {
 *     // do processing here...
 * }
 * ```
 * 
 * Once the `@Action` has this decoration, a menu item will be generated and the
 * `Action` will be assigned to its `action` property. All of the other menu 
 * item properties will then be set by the `Action` as described in the 
 * [JDK JavaDocs for Actions](https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/javax/swing/Action.html).
 * 
 * @see #owner() 
 * @see #position() 
 * @see #separatorAfter() 
 * @see #separatorBefore() 
 * @see Action
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 *
 * @version 1.0.0
 * @since 1.0.0
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.ANNOTATION_TYPE)
public @interface MenuItemProvider {
    
    /**
     * The `name` property holds the name of the method that this annotation is
     * decorating. This property's value will also give the menu item a name by
     * appending "MenuItem" to the name of the method.
     * 
     * @return the name for the method being decorated by this annotation
     */
    String name() default "";
    
    /**
     * The `owner` field tells the processor into which menu the menu item should
     * be added. This value needs to be the `text` property of the menu. So, for
     * example, if the menu item should be placed into the edit menu of the
     * application, the code to do so would be:
     * 
     * ```java
     * @MenuItemProvider(
     *     owner = "Edit",
     *     position = 300
     * )
     * ```
     * 
     * The text of the menu into which the menu item should be added is <em>case
     * sensitive</em>. Therefore, "Edit" and "edit" would be two different menus,
     * if both exist in the application's menu bar. Since it is the UI standards
     * that there never be two menus with the same text in the same application,
     * the text of the menu is sufficient for finding a menu to add a menu item.
     * 
     * @return the text of the menu into which this menu item should be added
     */
    String owner();
    
    /**
     * The `position` field tells the processor the desired position for the
     * menu item to be added to the `owner` menu. This value should be set in
     * increments of 100, starting at 100, to allow wiggle room for the processor
     * in the instance that two menu items desire the same position.
     * 
     * The `position` value should only be a positive whole number. The default
     * value is -1, which if not changed, tells the processor to place the menu
     * item in the first available position that makes sense. <em>This is not a 
     * form of AI, so it is better to set this value and not leave it up to the
     * processor</em>.
     * 
     * @return the position at which this menu item should be added in the menu
     */
    int position() default -1;
    
    /**
     * The `separatorBefore` property tells the processor if a separator should
     * be added to the menu before the menu item is added.
     * 
     * @return whether or not the menu item wants a separator placed before it
     */
    boolean separatorBefore() default false;
    
    /**
     * The `separatorAfter` property tells the processor if a separator should
     * be added to the menu after the menu item is added.
     * 
     * @return whether or not the menu item wants a separator placed after it
     */
    boolean separatorAfter() default false;
    
}
