/*
 * Copyright (C) 2021 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   AutoMenuPoC
 *  Class      :   module-info.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 26, 2021 @ 12:53:17 PM
 *  Modified   :   Oct 26, 2021
 * 
 *  Purpose:     See class JavaDoc comment.
 * 
 *  Revision History:
 * 
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 26, 2021  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

module GS.Platform {
    requires java.base;
    requires java.desktop;
    requires appframework;
    requires swing.worker;
    requires Menu.API;
    
    uses com.gs.menu.api.MenuItemProvider;
    uses com.gs.menu.api.MenuProvider;
    uses com.gs.menu.api.MenusNotAddedException;
    uses com.gs.menu.api.PositionableButton;
    uses com.gs.menu.api.PositionableMenu;
    uses com.gs.menu.api.PositionableMenuItem;
    uses com.gs.menu.api.RepeatableButtons;
    uses com.gs.menu.api.RepeatableMenus;
    uses com.gs.menu.api.ToolbarButtonProvider;
    
    exports com.gs.platform;
    
    provides com.gs.menu.api.RepeatableMenus with com.gs.platform.App;
    provides com.gs.menu.api.RepeatableButtons with com.gs.platform.TestPanelOne;
    
    opens com.gs.platform.resources to appframework;
}



