/*
 * Copyright (C) 2021 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   AutoMenuPoC
 *  Class      :   App.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 26, 2021 @ 12:54:23 PM
 *  Modified   :   Oct 26, 2021
 * 
 *  Purpose:     See class JavaDoc comment.
 * 
 *  Revision History:
 * 
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 26, 2021  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.gs.platform;

import com.gs.menu.api.ButtonsNotAddedException;
import com.gs.menu.api.MenuItemProvider;
import com.gs.menu.api.MenuItemsNotAddedException;
import com.gs.menu.api.MenuProvider;
import com.gs.menu.api.MenusNotAddedException;
import com.gs.menu.api.PositionableButton;
import com.gs.menu.api.PositionableMenu;
import com.gs.menu.api.PositionableMenuItem;
import com.gs.menu.api.RepeatableButtons;
import com.gs.menu.api.RepeatableMenus;
import com.gs.menu.api.ToolbarButtonProvider;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSeparator;
import javax.swing.JToolBar;
import org.jdesktop.application.Application;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 *
 * @version 1.0.0
 * @since 1.0.0
 */
@MenuProvider(name = "fileMenu", text = "File", position = Integer.MIN_VALUE)
@MenuProvider(name = "editMenu", text = "Edit", position = Integer.MIN_VALUE + 1)
@MenuProvider(name = "viewMenu", text = "View", position = Integer.MIN_VALUE + 2)
@MenuProvider(name = "toolsMenu", text = "Tools", position = Integer.MAX_VALUE - 1000)
@MenuProvider(name = "helpMenu", text = "Help", position = Integer.MAX_VALUE)
public class App extends Application {
    
    private final MainView mainView;
    private final JMenuBar menuBar;
    private final JToolBar toolBar;
    private final JPanel mainPanel;
    private final JPanel statusPanel;
    private final JLabel statusMessageLabel;
    private final JLabel animationIconLabel;
    private final JProgressBar progressBar;
    private static int returned = 0;
    
    public static RepeatableMenus provider(){
        RepeatableMenus provider = App.class.getAnnotation(RepeatableMenus.class);
        
        return provider;
    }

    public App () {
        mainView = new MainView(this);
        menuBar = new JMenuBar();
        menuBar.setName("menuBar");
        toolBar = new JToolBar();
        toolBar.setName("toolBar");
        mainPanel = new JPanel();
        mainPanel.setName("mainPanel");
        statusPanel = new JPanel();
        statusPanel.setName("statusPanel");
        statusMessageLabel = new JLabel();
        statusMessageLabel.setName("statusMessageLabel");
        animationIconLabel = new JLabel();
        animationIconLabel.setName("animationIconLabel");
        progressBar = new JProgressBar();
        progressBar.setName("progressBar");
    }
    
    @Override
    protected void initialize(String[] args) {
        MainView mainView = new MainView(this);
        
        try {
            // The first thing we will do is create the application menu bar.
            createMenuBar();
            
            // Next, we will add buttons to the application's toolbar.
            //+
            //+ Since ToolbarButtonProviders can also (though not necessarily)
            //+ provide menu items, we receive the menu items that may have been
            //+ created as a HashMap, with the menu items as the keys and the
            //+ owner menu text as the value. This is because there may be many
            //+ menu items being placed in the same menu and HashMap keys *must*
            //+ be unique.
            HashMap<PositionableMenuItem, String> menuItems = createToolbar();
            
            // Now that we have gotten the toolbar buttons added, we have a map
            //+ that has some menu items in it. However, there may be some 
            //+ actions out there that have menu items, but not toolbar buttons.
            //+ Those actions will be decorated with MenuItemProvider, so we
            //+ need to get those as well.
            menuItems = getMoreMenuItems(menuItems);
            
            if (menuItems != null) {
                addMenuItems(menuItems);
            }
        } catch (MenusNotAddedException | ButtonsNotAddedException
                | MenuItemsNotAddedException e) {
            System.out.println("=".repeat(80));
            System.err.println("Error Message: " + e.getMessage());
            System.err.println("Exception Type: " + e.getClass().toString());
            System.err.println("-".repeat(80));
            System.err.println("Stack Trace:");
            
            for (StackTraceElement el : e.getStackTrace()) {
                System.err.println("\t" + el.toString());
            }
        }
    }

    @Override
    protected void startup() {
        MainView mainView = new MainView(this);
        mainView.getFrame().addWindowListener(new MainWindowAdapter());
        mainView.setMenuBar(menuBar);
        mainView.setToolBar(toolBar);
        mainView.setComponent(mainPanel);
        mainView.setStatusBar(statusPanel);
        
        statusMessageLabel.setText("Application message...");
        mainView.getStatusBar().add(statusMessageLabel);
        
        show(mainView);
    }
    
    private void createMenuBar() throws MenusNotAddedException {
        // Get references to all of the MenuProviders in the application. This
        //+ is done by getting a reference to all of the RepeatableMenus in the
        //+ application first. Then we will get the MenuProviders.
        ServiceLoader<RepeatableMenus> menusLoader = ServiceLoader.load(RepeatableMenus.class);
        
        // First, we need to turn each of the MenuProviders into JMenus.
        //+ However, we need to be able to compare the positions of said JMenus
        //+ in order to make sure that our menu bar is built correctly. To do
        //+ this, we will turn each of the MenuProviders into custom Menus that
        //+ contain a position property. These custom menus are called PositionableMenu.
        List<PositionableMenu> menus = new ArrayList<>();
        
        for (RepeatableMenus p : menusLoader) {
            for (MenuProvider m : p.value()) {
                PositionableMenu menu = new PositionableMenu(m.name(), m.text(), m.position());
                menus.add(menu);
            }
        }
        
        // Now, we need to verify that all menus have been added and not the 
        //+ same menu again and again.
        if (menus.get(0).equals(menus.get(menus.size() - 1))) {
            throw new MenusNotAddedException("only the last menu is present");
        }
        
        // Now that we are sure the menus were added properly, we can sort them.
        Collections.sort(menus);
        
        // Finally, we can add the top-level menus to the menu bar.
        menus.forEach(m -> {
            menuBar.add(m);
        });
    }
    
    private HashMap<PositionableMenuItem, String> createToolbar() 
            throws ButtonsNotAddedException {
        HashMap<PositionableMenuItem, String> rv = new HashMap<>();
        
        // Get references to all of the ToolbarButtonProviders in the application
        ServiceLoader<RepeatableButtons> buttonsLoader = ServiceLoader.load(RepeatableButtons.class);
        List<PositionableButton> buttons = new ArrayList<>();
        
        // First, we need to turn each ToolbarButtonProvider into a JButton.
        //+ However, we need to be able to compare the positions of said JButtons
        //+ in order to add the correctly. To do this, we will turn each of the
        //+ ToolbarButtonProviders into our custom PositionableButtons. This way
        //+ we will be able to sort them by their position property prior to 
        //+ adding them to the toolbar.
        for (RepeatableButtons b : buttonsLoader) {
            for (ToolbarButtonProvider p : b.value()) {
                // Add the PositionalButton to the list
                buttons.add(getButtonFromButtonAnnotation(p));

                if (p.providesMenuItem()) {
                    // Add the PositionalMenuItem to the map
                    rv.put(getMenuItemFromButtonProvider(p), p.owner());
                }
            }
        }
        
        // Now, we need to verify that all buttons have been added and not the
        //+ same button again and again.
        if (!buttons.isEmpty()) {
            if (buttons.get(0).equals(buttons.get(buttons.size() - 1))) {
                throw new ButtonsNotAddedException("only the last button is present");
            }
        
            // Now that we are sure the buttons were properly added, we can sort them.
            Collections.sort(buttons);


            // Once sorted, we can add tehm to the application toolbar.
            buttons.forEach(b -> {
                if (b.hasSeparatorBefore()) {
                    toolBar.add(new JSeparator());
                }
                
                toolBar.add((JComponent) b);
                
                if (b.hasSeparatorAfter()) {
                    toolBar.add(new JSeparator());
                }
            });
        }
        
        // Now we need to return the HashMap of menu items so that more can be
        //+ added to it if any MenuItemProviders are found.
        return (rv.isEmpty()) ? null : rv;
    }
    
    private PositionableMenuItem getMenuItemFromButtonProvider(
            ToolbarButtonProvider btn) {
        // First, we need to create an instance of the PositionableMenuItem.
        //+ REMEMBER that the only name property in the ToolbarButtonProvider is
        //+ the name of the action method, so when passing the name for the menu
        //+ item, we need to append "MenuItem" to it.
        PositionableMenuItem m = new PositionableMenuItem(btn.name() 
                + "MenuItem", btn.menuItemPosition(), btn.separatorBeforeMenuItem(),
                btn.separatorAfterMenuItem());
        
        // Next, we need to get the javax.swing.Action to assign to the menu 
        //+ items action property. To do this, we just need to call the get()
        //+ method of the ActionMap and pass btn.name() to it.
        m.setAction(
                getContext().getActionManager().getActionMap().get(btn.name()));
        
        // Finally, return the newly created PositionableMenuItem.
        return m;
    }
    
    private PositionableButton getButtonFromButtonAnnotation(
            ToolbarButtonProvider btn) {
        // First, create a PositionableButton instance. REMEMBER, if 
        //+ textOverride.equals("-1"), the text is set by the Action that will
        //+ be assigned to the button's action property. However, this is taken
        //+ care of in the constructor of the PositionableButton. Therefore, we
        //+ simply send all of the information to the constructor.
        //+
        //+ ALSO, remember to add "Button" to the end of the btn.name() property,
        //+ because the value is actually the method name.
        PositionableButton button = new PositionableButton(btn.name() 
                + "Button", btn.textOverride(), btn.position(),
                btn.separatorBefore(), btn.separatorAfter());
        
        // Next, we need to get the javax.swing.Action for which the decorated
        //+ method will be the actionPerformed method. However, that is a 
        //+ difficult task, so the ToolbarButtonProvider method has as its name
        //+ property variable the method name. By providing the btn.name()
        //+ to the ActionMap.get() method, the javax.swing.Action will be 
        //+ returned and assigned to the button's action property.
        button.setAction(
                getContext().getActionManager().getActionMap().get(btn.name()));

        return button;
    }
    
    private void addMenuItems(HashMap<PositionableMenuItem, String> map) 
            throws MenuItemsNotAddedException {
        // We have received a (hopefully) populated HashMap that contains all of
        //+ the menu items provided by either the 
        //+ ToolbarButtonProvider.providesMenuItems and/or the menu items supplied
        //+ by the MenuItemProviders. So, we will begin by making sure the map
        //+ is not null and not empty.
        if (map != null && !map.isEmpty()) {
            // We are going to get an array of the map keys. Then we will compare
            //+ the first map key with the last map key to be sure we did not
            //+ just keep readding the same menu item. If we did, our map would
            //+ only have a single entry.
            String[] keysArray = (String[]) map.keySet().toArray();
            if (keysArray[0].equals(keysArray[keysArray.length - 1])) {
                throw new MenuItemsNotAddedException("only the last menu item "
                        + "is present");
            }

            // Let's sort the HashMap before we do anything else. To do this, we
            //+ had to create a custom sorting method because we are sorting on
            //+ the values, instead of the keys. The reason for this is that we
            //+ want all of the File owners together, all of the Edit owners 
            //+ together, and so on. This will make sense in just a bit.
            HashMap sortedMap = sortByValue(map);
            
            // Now that the map is sorted by values, we need to get all of the
            //+ menu items for a single menu into a list that we can then sort
            //+ by position. To accomplish this, we will need to get the keyset
            //+ for the map.
            Set<PositionableMenuItem> keys = map.keySet();
            
            // With the keyset in hand, we can now loop through all of the keys
            //+ to check for their owner menu. As we find the same owner menus,
            //+ we will add those keys to the unsorted list.
            String currentMenu = null;
            for (PositionableMenuItem key : keys) {
                List<PositionableMenuItem> sortable = new ArrayList<>();
                
                if (currentMenu == null) {
                    currentMenu = map.get(key);
                }
                
                if (currentMenu.equals(map.get(key))) {
                    // Since they are in the same menu, we can add the menu item
                    //+ to the sortable list.
                    sortable.add(key);
                    
                    // Now that we have added the key to the sortable list, we
                    //+ no longer need it in the map, so remove it.
                    map.remove(key);
                } else {
                    // We have gotten all of the menu items for a single menu
                    //+ into our list. So now we need to sort it by position.
                    Collections.sort(sortable);
                    
                    // With the list sorted, we can add these menu items to the
                    //+ menu to which they belong.
                    addItemsToMenu(sortable, currentMenu);
                    
                    // Now that all menu items have been added to the menu bar's
                    //+ top-level menus, we can recurse this method since we 
                    //+ have been removing the items from the map.
                    if (!map.isEmpty()) {
                        addMenuItems(map);
                    }
                    // If the map is empty, we do not want to recurse. Instead,
                    //+ we want to get on with our lives.
                }
            }
        }
    }
    
    private void addItemsToMenu(List<PositionableMenuItem> list, String menu) {
        // To find the menu into which the list of items needs to be added, we
        //+ will loop through all of the menus and check their text property.
        for (int x = 0; x < menuBar.getMenuCount(); x++) {
            if (menu.equals(menuBar.getMenu(x).getText())) {
                for (PositionableMenuItem m : list) {
                    if (m.hasSeparatorBefore()) {
                        menuBar.getMenu(x).add(new JSeparator());
                    }
                    
                    menuBar.getMenu(x).add(m);
                    
                    if (m.hasSeparatorAfter()) {
                        menuBar.getMenu(x).add(new JSeparator());
                    }
                }
                
                // Now that we have added all of our menu items to this menu,
                //+ we can get out of here.
                break;
            }
        }
    }
    
    private HashMap<PositionableMenuItem, String> sortByValue(
            HashMap<PositionableMenuItem, String> hm) {
        // Create a list from elements of HashMap
        List<Map.Entry<PositionableMenuItem, String>> list 
                = new LinkedList<Map.Entry<PositionableMenuItem, String>>(hm.entrySet());
        
        // Sort the list
        Collections.sort(list, new Comparator<Map.Entry<PositionableMenuItem,
                String>>() {
            @Override
            public int compare(Map.Entry<PositionableMenuItem, String> o1, 
                    Map.Entry<PositionableMenuItem, String> o2) {
                return (o1.getValue().compareTo(o2.getValue()));
            }
            
        });
        
        // Put data from sorted list into HashMap
        HashMap<PositionableMenuItem, String> temp = new LinkedHashMap<>();
        
        for (Map.Entry<PositionableMenuItem, String> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        
        return temp;
    }
    
    private HashMap<PositionableMenuItem, String> getMoreMenuItems(
            HashMap<PositionableMenuItem, String> map) 
            throws MenuItemsNotAddedException {
        // First, we need to load all menu items that were not loaded by the 
        //+ createToolbar method. This is because there may be some menu items
        //+ that do not need a toolbar button associated with them, so they are
        //+ created using the MenuItemProvider, instead of the 
        //+ ToolbarButtonProvider. These are the menu items we are after now.
        ServiceLoader<MenuItemProvider> menuItemsLoader = ServiceLoader.load(
                MenuItemProvider.class);
        
        // Next, we need to turn each MenuItemProvider into a JMenuItem. However,
        //+ the JMenuItem does not provide a means for sorting based upon the
        //+ desired position, so we will create instance of the 
        //+ PostitionableMenuItem class. This way, we will be able to sort the
        //+ supplied menu items so that they can be loaded in the various menus
        //+ at the desired positions.
        for (MenuItemProvider m : menuItemsLoader) {
            PositionableMenuItem p = new PositionableMenuItem(m.name() 
                    + "MenuItem", m.position(), m.separatorBefore(), 
                    m.separatorAfter());
            
            // Now, we need to get the javax.swing.Action that should be assigned
            //+ to the menu item. We can get this from the m.name(), as that
            //+ should be the action method name.
            p.setAction(
                    getContext().getActionManager().getActionMap().get(m.name()));
            
            // Now that the action has been assigned, we are ready to add the
            //+ menu item to our map. We do so with the menu item as the key
            //+ (because it is unique and the owner is not), and the owner as 
            //+ the value.
            //+
            //+ Also, it is worthy to note that the map provided to this method
            //+ is already loaded with the menu items that were included with
            //+ the toolbar buttons, so we are simply adding them to the end of
            //+ the map.
            map.put(p, m.owner());
        }
        
        // Last, we return the map that was passed to us, with the new menu items
        //+ added to it.
        return map;
    }
    
    private class MainWindowAdapter extends WindowAdapter {
        
        @Override
        public void windowClosing(WindowEvent e) {
            exit(e);
        }
    }

}
